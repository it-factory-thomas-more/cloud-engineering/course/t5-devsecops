# 🎓T5 - DevSecOps

![DevSecOps cartoon](2023-10-26-22-04-56.png){width=25%}

## The need for security in DevOps

One of the main principles of DevOps is to automate as much as possible. This is great to increase release frequency and decrease lead times, but it can also introduce additional security risks. If you have a continuous deployment pipeline that automatically provisions infrastructure and deploys code, you need to make sure that the code and infrastructure are secure. Some examples of possible threat scenarios:

- A developer or cloud engineer might accidentally introduce a security vulnerability in the code or infrastructure.
- A malicious actor might use your pipeline to deploy malicious code or infrastructure.

### The SolarWinds hack

In December 2020, it was discovered that the SolarWinds Orion software had been compromised. This software is used by many organizations to monitor their IT infrastructure. The attackers managed to inject malicious code into the software, which was then distributed to SolarWinds customers. This code was then used to gain access to the customers' networks.

The Solarwinds breach is seen as the biggest supply chain attack in history. Through this method, a nation state actor, allegedly Russia, was able to gain access to many organizations, including the US government, FireEye, Microsoft, Cisco, Intel, and many more.

As the excellent Palo Alto Network ["Anatomy of a Cloud Supply Pipeline Attack"](https://www.paloaltonetworks.com/cyberpedia/anatomy-ci-cd-pipeline-attack#:~:text=Once%20attackers%20gain%20access%20to,presence%20of%20container%20escape%20operations.) article states, the attackers used the "Exponential Growth Model" to gain access to many organizations. Through the distribution of the malicious code via Service Providers and their customers, they were able to infiltrate about 10,000 organizations.

![Palo Alto Exponential growth model](2023-10-25-12-38-22.png){width=50%}

How were the attackers able to inject this malicious code?

From the [Cyberark "The Anatomy of the SolarWinds Attack Chain" blog post](https://www.cyberark.com/resources/blog/the-anatomy-of-the-solarwinds-attack-chain):

>To establish a foothold into the organization, the threat actor compromised the “heart” of the CI/CD pipeline, where code is tested, wrapped, containerized and signed, then successfully changed SolarWinds’ source code. The attacker deployed malware, now infamously known as “SunSpot,” which ran with high privileges, scanning for Orion builds.

>With surgical precision – and without tipping off developers or engineers – the malware changed source file code names (ever so slightly) to deploy a backdoor. Following the build update, the backdoor code was deleted and the original file names were restored.

How did they get access to the CI/CD pipeline? Apparently, Solarwinds had some source code on a Github public repository, which contained the FTP credentials to their update server. Through there, they got access to the CI/CD pipeline.

This breach was a wake-up call for many organizations. It showed that even the most trusted software providers can be compromised, and that it is important to secure your CI/CD pipelines. To be able to do that, we must first know what the most common security risks are in CI/CD pipelines.

## OWASP Top 10 CI/CD Security Risks

The [OWASP Top 10 CI/CD Security Risks](https://owasp.org/www-project-top-10-ci-cd-security-risks) is a list of the most common security risks in CI/CD pipelines. It is based on the OWASP Top 10, but adapted to the CI/CD context. The list was created by the OWASP Top 10 Proactive Controls project.

![OWASP Top 10 CI/CD Security Risks](2023-10-26-10-09-31.png){width=50%}

### OWASP CICD-SEC-1: Insufficient Flow Control Mechanisms

This refers to the ability of an attacker that has obtained permissions to a system within the CI/CD process (SCM, CI, Artifact repository, etc.) to single-handedly push malicious code or artifacts down the pipeline, due to a lack of mechanisms that enforce additional approval or review.
- An attacker can abuse this flaw by pushing code or artifacts to a repository branch, triggering a pipeline that ships the code to production, uploading an artifact to an artifact repository, or directly changing application code or infrastructure in production, without any approval or review.
- The potential impact of this flaw is that an attacker can deploy malicious artifacts that can compromise the production environment, exfiltrate sensitive data, execute remote commands, create backdoors, or cause denial of service.
- The recommendations to reduce the risk of this flaw are:
    - Implement mechanisms to approve or review changes to application and infrastructure code before they are deployed to production.
    - Configure branch protection rules on branches hosting code which is used in production and other sensitive systems
    ![Protected master branch](2023-10-26-11-07-35.png){width=50%}
    - Avoid exclusion of user accounts or branches from branch protection rules.
    - Ensure that user accounts that can push unreviewed code to a repository do not have the permission to trigger the deployment pipelines connected to the repository.
    - Establish pipeline flow control mechanisms to ensure that no single entity (human / programmatic) is able to ship sensitive code and artifacts through the pipeline without external verification or validation.


### OWASP CICD-SEC-2: Inadequate Identity and Access Management

This concerns the challenges and risks of managing the vast number of identities (both human and programmatic) across the different systems in the CI/CD ecosystem, such as SCM, CI, Artifact repository, etc.
- An attacker can abuse this flaw by compromising an identity that has excessive or unnecessary permissions, accessing sensitive systems or data, injecting malicious code or artifacts, or impersonating other identities.
- The potential impact of this flaw is that an attacker can gain unauthorized access to the CI/CD systems and processes, tamper with the code or artifacts, steal confidential information, create backdoors, or cause denial of service.
- The recommendations to reduce the risk of this flaw are:
    - Implement a identity and access management system and/or policies that can enforce consistent security policies and deprovisioning across all CI/CD systems.
    - Apply the principle of least privilege to all identities and limit their access to the minimum required resources and actions.
    - Use strong authentication methods such as MFA and SSH keys for all identities and avoid using local or external accounts that are not managed by the organization.
    - Monitor and audit the activities and permissions of all identities and detect any anomalies or misuse.

### OWASP CICD-SEC-3: Dependency Chain Abuse

This means that an attacker can exploit how code dependencies are fetched, which allows them to run malicious code after a package has been pulled.
- An attacker can take advantage of this flaw by publishing fake or hijacked packages with the same or similar names as legitimate ones, and tricking developers or build systems into downloading and executing them.
- The possible impact of this flaw is that an attacker can run malicious code on the machines that pull the packages, and compromise the production environment, steal sensitive data, create backdoors, or cause denial of service.
- The suggestions to mitigate this flaw are:
    - Use a centralized and secure system to manage dependencies and external packages.
    - Verify the integrity and authenticity of the packages before pulling them.
    - Monitor and audit the dependency chain and detect any anomalies or malicious activities.

In December 2022, a [malicious dependency was introduced into the code of the popular Python machine learning library pytorch](https://www.bleepingcomputer.com/news/security/pytorch-discloses-malicious-dependency-chain-compromise-over-holidays/).

![Pip torchitron](2023-10-26-11-57-34.png){width=50%}

>Between December 25th and December 30th, 2022, users who installed PyTorch-nightly should ensure their systems were not compromised, PyTorch team has warned.
The warning follows a 'torchtriton' dependency that appeared over the holidays on the Python Package Index (PyPI) registry, the official third-party software repository for Python.

>"Please uninstall it and torchtriton immediately, and use the latest nightly binaries (newer than Dec 30th 2022)," advises PyTorch team.

>The malicious 'torchtriton' dependency on PyPI shares name with the official library published on the PyTorch-nightly's repo. But, when fetching dependencies in the Python ecosystem, PyPI normally takes precedence, causing the malicious package to get pulled on your machine instead of PyTorch's legitimate one.

>"Since the PyPI index takes precedence, this malicious package was being installed instead of the version from our official repository. This design enables somebody to register a package by the same name as one that exists in a third party index, and pip will install their version by default," writes PyTorch team in a disclosure published yesterday.


### OWASP CICD-SEC-4: Poisoned Pipeline Execution

In this case, an attacker who only has access to the source repository and not to the build environment can modify the pipeline configuration file or other files used by the pipeline to run malicious code during the build process.

- An attacker can exploit this flaw by accessing the source control system and changing the pipeline configuration file or other files that contain commands or scripts executed by the pipeline. The attacker can also submit a pull request with the malicious changes and trigger pipeline execution.
- The possible impact of this flaw is that an attacker can run malicious code on the build node and compromise the production environment, steal confidential information, create backdoors, or cause denial of service.
- The suggestions to prevent this flaw are:
    - Protect the pipeline configuration file and other files used by the pipeline from unauthorized changes. This can be done by only granting access to trusted users. You can also use remote branches to store the pipeline configuration file and other files used by the pipeline.
    - Review and approve any changes to the pipeline configuration file and other files used by the pipeline before executing them. Use only protected branches to launch your pipeline from.
    - Detect and alert on any anomalies or malicious activities in the pipeline execution.

The [Examples part of the OWASP CICD-SEC-4 page](https://owasp.org/www-project-top-10-ci-cd-security-risks/CICD-SEC-04-Poisoned-Pipeline-Execution) provides some very insightful examples of how this can be exploited.


### OWASP CICD-SEC-5: Insufficient PBAC (Pipeline-Based Access Controls)

This risk refers to the permissions of nodes that are executing the pipeline. If an attacker can gain access to a node that is executing the pipeline, they can potentially access and modify secrets, source code, artifacts, or other pipelines.

 While we are mostly using cloud-based Gitlab runners to execute pipeline tasks, a lot of organizations are deploying these runners on their own infrastructure, for security, compliance, financial or performance reasons. This diagram shows a setup where the Gitlab runners are deployed on Amazon EC2 instances.

 ![Gitlab runners on EC2](2023-10-31-11-57-16.png){width=50%} [^gitlabawsrunnersrc]
 
[^gitlabawsrunnersrc]: [Gitlab runners on EC2](https://aws.amazon.com/blogs/devops/deploy-and-manage-gitlab-runners-on-amazon-ec2/)
 
 When we are installing these runners on VMs in our own datacenter or on instances in the cloud, we need to make sure that these VMs have properly configured permissions in order to prevent a malicious actor from accessing the various resources that are used by the pipeline.

- An attacker can exploit this flaw by running malicious code in the pipeline execution node and leveraging the access to secrets, source code, artifacts, or other pipelines. The attacker can also move laterally within or outside the CI/CD system and reach other servers or systems.
- The possible impact of this flaw is that an attacker can compromise the production environment, steal confidential information, create backdoors, or cause denial of service.
- The suggestions to prevent this flaw are:
    - Use separate nodes for pipelines with different levels of sensitivity or access requirements.
    - Restrict the secrets and resources that each pipeline and step can access according to the principle of least privilege.
    - Reset the execution node to its original state after each pipeline execution.
    - Monitor and alert on any anomalies or malicious activities in the pipeline execution.

### OWASP CICD-SEC-6: Insufficient Credential Hygiene

This concerns the situation where the credentials used in the CI/CD pipeline are not managed and protected properly, and can be exposed or stolen by attackers.
- An attacker can exploit this flaw by finding hard-coded credentials in the source-controlled code, accessing shared keys or weak passwords inside pipeline code, downloading container images with credentials in them, or viewing console output with credentials in them.
- The possible impact of this flaw is that an attacker can access or modify sensitive systems or data in the CI/CD environment, compromise the production environment, create backdoors, or cause denial of service.
- The suggestions to prevent this flaw are:
    - Use a secure and centralized system to store and manage credentials and secrets. Refer to [Secrets management](#secrets-management) for more information.
    - Use [pre-commit hooks](#pre-commit-hooks) and [secrets detection](#secrets-detection) tools to prevent secrets from being committed to source control.
    - Use strong and unique passwords and keys for each credential and rotate them frequently.
    - Avoid hard-coding credentials in the code or printing them to the console output.
    - Check the integrity and authenticity of the container images before using them.

### OWASP CICD-SEC-7: Insecure System Configuration

It means that the security settings, configuration and hardening of the different systems in the CI/CD pipeline (e.g. SCM, CI, Artifact repository) are flawed, and can be exploited by attackers to gain access or compromise the systems. 

Again, these systems can be cloud-based or on-premise. For example, if we are using a self-hosted Gitlab instance, we need to make sure that it is properly configured and hardened.

- An attacker can take advantage of this flaw by finding and abusing vulnerabilities or misconfigurations in the systems, such as outdated versions, weak network controls, default credentials, insecure system settings, and more.
- The possible impact of this flaw is that an attacker can access or modify sensitive data or resources in the CI/CD environment, tamper with the code or artifacts, compromise the production environment, create backdoors, or cause denial of service.
- The suggestions to prevent this flaw are:
    - Keep an inventory of systems and versions in use, and update and patch them regularly.
    - Apply strong network and application security controls to all systems.
    - Use a secure and centralized system to store and manage credentials and secrets.
    - Check and optimize the security settings and configurations of each system.

### OWASP CICD-SEC-8: Ungoverned Usage of 3rd Party Services

This scenario targets the 3rd party services that are connected to the CI/CD systems and processes are not controlled or monitored properly, and can pose a threat to the security and integrity of the CI/CD ecosystem. It is a clear example of a supply chain attack.
- An attacker can take advantage of this flaw by gaining access to the 3rd party services that have high level of permissions or access to the CI/CD systems, such as SCM, CI, Artifact repository, etc. The attacker can then use the 3rd party services to access or modify sensitive data or resources, inject malicious code or artifacts, or impersonate other identities.
- The possible impact of this flaw is that an attacker can compromise the CI/CD systems and processes, tamper with the code or artifacts, steal confidential information, create backdoors, or cause denial of service.
- The suggestions to prevent this flaw are:
    - Establish proper governance controls for all 3rd party services that are connected to the CI/CD systems and processes. These controls include approval, visibility during ongoing usage, and deprovisioning procedures and policies.

A real-world example of this is the [Codecov attack](https://blog.gitguardian.com/codecov-supply-chain-breach/) which effected thousands of organizations.

Codecov is a code coverage service, which can be used to see how much of your application is being tested. They supplied a tool that could be used inside the customers CI/CD pipelines to send the code coverage data to Codecov. The attackers got access and modified the Docker image that built the tool. This modified and malicious version was downloaded and executed by the customers' CI/CD pipelines, and used it to steal credentials and secrets from their environments.

![Codecov attack](2023-10-31-12-36-49.png){width=50%} [^codecovattacksrc]

[^codecovattacksrc]: [Codecov attack](https://blog.gitguardian.com/codecov-supply-chain-breach/)

### OWASP CICD-SEC-9: Improper Artifact Integrity Validation

This refers to the automated verification of the artifact integrity within the CI/CD process. the Solarwinds and Codecov attacks could probably have been prevented if the integrity of the artifacts was verified before they were used in the next stage of CI/CD process.

Without this kind of verification, the artifacts that are fed between stages can be tampered with by an attacker who has access to one of the systems in the CI/CD process.
- An attacker can abuse this flaw by pushing malicious code or artifacts down the pipeline, without any validation or verification mechanisms to detect or prevent them. The attacker can also exploit vulnerabilities or misconfigurations in the systems that fetch or store code and artifacts from external sources.
- The potential impact of this flaw is that an attacker can run malicious code on the systems within the CI/CD process or in production, and compromise the security and integrity of the CI/CD ecosystem. The attacker can also access or modify sensitive data or resources, create backdoors, or cause denial of service.
- The recommendations to prevent this flaw are:
    - Use processes and technologies to validate the integrity of code and artifacts from development to production. For example, use code signing, artifact verification software, and configuration drift detection.
    - Verify the integrity and authenticity of external resources before using them in the pipeline. For example, check the hash of the resource against the official published hash of the resource provider.

### OWASP CICD-SEC-10: Insufficient Logging and Visibility

The ["Detect" function of the NIST Cybersecurity Framework](https://www.nist.gov/cyberframework/online-learning/five-functions#detect) states that "Anomalies and events are detected in a timely manner and the potential impact of events is understood." This is also true for CI/CD pipelines. If we do not have proper logging and visibility in our CI/CD pipelines, we will not be able to detect anomalies and events in a timely manner, and we will not be able to understand the potential impact of these events.

- This is caused by CI/CD systems and processes that do not generate or store enough logs to detect or investigate a security incident, and an attacker can thus carry out malicious activities without being noticed or traced.
- An attacker can exploit this flaw by accessing or modifying the CI/CD systems and processes, injecting malicious code or artifacts, stealing confidential information, creating backdoors, or causing denial of service, without leaving any evidence or clues.
- The possible impact of this flaw is that an attacker can compromise the security and integrity of the CI/CD ecosystem, and the organization may fail to detect a breach, mitigate the damage, or identify the root cause.
- The suggestions to prevent this flaw are:
    - Identify and inventory all the systems and processes involved in the CI/CD pipeline, and enable all relevant logs for them.
    - Use a centralized and secure system to collect and store logs from all sources.
    - Analyze and correlate logs to detect any anomalies or malicious activities in the CI/CD pipeline.
    

## DevSecOps Controls

Microsoft has developed a set of DevSecOps controls that can be used to secure your DevOps activities. These can be used to implement the recommendations from the OWASP Top 10 CI/CD Security Risks section.

![Microsoft DevSecOps controls](2023-10-26-11-21-55.png){width=50%} [^msdevsecopscontrolssrc]

[^msdevsecopscontrolssrc]: [Microsoft DevSecOps controls](https://docs.microsoft.com/en-us/azure/devops/devops-essentials/devops-devsecops-controls?view=azure-devops)

#### Secrets detection

We want to prevent secrets from being pushed to a Gitlab repository. Gitlab has a [built-in secrets detection tool](https://docs.gitlab.com/ee/user/application_security/secret_detection/) that can be used to scan your source code for secrets.


### Pre-commit hooks

Gitlab's built-in secrets detection tool is great, but it only works when you push your code to the repository and it is already *too late*. It would be better if we could prevent secrets from being committed to source control in the first place. This is where pre-commit hooks come in.

Pre-commit hooks are scripts that are executed before a commit is made. They can be used to prevent secrets from being committed to source control, and to enforce code quality standards.

An often used example is [git-secrets](https://github.com/awslabs/git-secrets), which is a tool that can be used to prevent secrets from being committed to source control. It does this by scanning the files that are being committed for secrets. If a secret is found, the commit is aborted.

**✅TASK:** Create a copy of your Terraform multi-cloud challenge repo. Install `git-secrets` on your machine. Configure it to scan for AWS secrets. Try to a file that contains a (fake) AWS secret to your repository. 

You should not be allowed to commit the secret and see something like this. This example uses `git` inside VSCode, but it should work in the terminal as well. Give it a try!
![git-secrets result](2023-10-31-16-39-51.png){width=50%}

### Static Application Security Testing (SAST)

Gitlab has built-in SAST scanning capabilities. You can use the built-in SAST scanning capabilities to scan your application source code for security issues. The built-in SAST scanning capabilities supports a lot of languages and frameworks, including Python, Java, Javascript, Go, Ruby, PHP, C/C++, and more.

**✅TASK:** Activate SAST on your repo by adding the following lines to the .gitlab-ci.yml file:

```yaml
include:
  - template: Jobs/SAST.gitlab-ci.yml
```

The job will run in the `test` stage of your pipeline. Commit and push your changes to Gitlab. You should see a new job in your pipeline. The stage outputs a json artifact that contains the results of the scan, which is automatically imported into the Gitlab Security Dashboard.

![Security dashboard SAST](2023-10-31-20-41-30.png){width=50%}

As you can see, the Dockerfile for the Hangman API has a security vulnerability. Let's see the details for this vulnerability.

![Security Dashboard triage](2023-10-31-20-43-49.png){width=50%}

Notice that you can immediately triage the vulnerability from the Security Dashboard and Vulnerability Report. You can confirm that this is an issue that has to be solved, mark it as a false positive or an accepted risk, or dismiss it with a valid reason. Let's confirm this as an issue that has to be solved.

Now fix the issue inside the Dockerfile. You can find some help [here](https://snyk.io/blog/best-practices-containerizing-python-docker/).

When the issue is fixed (PS: make sure your code still works!), commit and push your changes to Gitlab. The vulnerability won't be automatically deleted from the Security Dashboard. You will have to verify that it wasn't detected anymore during the last pipeline run. To do that, you can view the security report specificly for the latest pipeline run by opening the pipeline and clicking on the Security tab.

![Pipeline security report](2023-10-31-21-19-30.png){width=50%}

When the vulnerability is not detected anymore, you can conclude that the issue has been fixed and mark it as resolved in the Security Dashboard.

### Dynamic Application Security Testing (DAST)

Gitlab has [built-in DAST scanning](https://docs.gitlab.com/ee/user/application_security/dast/) capabilities. You can use these to scan your application for security issues. It supports a lot of languages and frameworks, including Python, Java, Javascript, Go, Ruby, PHP, C/C++, and more.


### Dependency Management

Gitlab also has [built-in dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) capabilities. You can use these to scan your application source code for security issues. A lot of languages and frameworks are supported, including Python, Java, Javascript, Go, Ruby, PHP, C/C++, and more.

### Secure pipelines

This refers to the security of the CI/CD pipeline itself. A lot of the recommendations in the OWASP Top 10 CI/CD Security Risks section are related to this, like using protected branches, restricting access to source control, following the least privilege principle and using proper secrets management.

### Secrets management

With the plethora of secrets that are used in a CI/CD pipeline, it is recommended to have proper secrets management in place. Until now, we have been using environment variables to store secrets. This is not a good practice, as environment variables are not encrypted and can be easily accessed by anyone with access to the build system.

A better solution is to use a dedicated secrets management tool like [Hashicorp Vault](https://www.vaultproject.io/). Vault can be used to store secrets and provide them to the CI/CD pipeline when needed. It can also be used to generate dynamic secrets, which are only valid for a certain time period.

Cloud providers also provide their own secrets management tools. For example, AWS provides [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/), which can be used to store secrets and provide them to the CI/CD pipeline when needed. Azure provides [Azure Key Vault](https://azure.microsoft.com/en-us/services/key-vault/), which can be used to store secrets and provide them to the CI/CD pipeline when needed.

### Cloud configuration validation and Infrastructure scanning

As we are defining our infrastructure as code, we can use the same kind of SAST tools that we use for application code scanning to scan our infrastructure code. This can help us detect security issues in our infrastructure code before we deploy it.

Container images regularly contain outdated packages and vulnerabilities. It is important to scan container images for vulnerabilities before deploying them.

Created by Bridgecrew, [Checkov](https://www.checkov.io/) is an open source policy-as-code tool that scans for security issues in infrastructure as code (IaC) templates, container images, and pipeline configuration. It supports Terraform, CloudFormation, Kubernetes, and more.

**✅TASK:** Create a job for the Checkov scanner in the `.gitlab-ci.yml` file. You can find some guidelines [here](https://www.checkov.io/4.Integrations/GitLab%20CI.html). The results of the scan are in xml format. You can find them among the artifacts of the pipeline run.

```xml
...
<testcase name="[NONE][CKV_AWS_91] Ensure the ELBv2 (Application/Network) has access logging enabled" classname="/aws_autoscaling.tf.aws_alb.asg_alb" file="/aws_autoscaling.tf">
<failure type="failure" message="Ensure the ELBv2 (Application/Network) has access logging enabled"> Resource: aws_alb.asg_alb File: /aws_autoscaling.tf: 72-81 Guideline: https://docs.prismacloud.io/en/enterprise-edition/policy-reference/aws-policies/aws-logging-policies/bc-aws-logging-22 72 | resource "aws_alb" "asg_alb" { 73 | name = "asg-alb" 74 | internal = false 75 | load_balancer_type = "application" 76 | security_groups = [aws_security_group.alb_sg.id] 77 | subnets = module.vpc.public_subnets 78 | tags = { 79 | Name = "asg-alb" 80 | } 81 | } </failure>
</testcase>
<testcase name="[NONE][CKV_AWS_131] Ensure that ALB drops HTTP headers" classname="/aws_autoscaling.tf.aws_alb.asg_alb" file="/aws_autoscaling.tf">
<failure type="failure" message="Ensure that ALB drops HTTP headers"> Resource: aws_alb.asg_alb File: /aws_autoscaling.tf: 72-81 Guideline: https://docs.prismacloud.io/en/enterprise-edition/policy-reference/aws-policies/aws-networking-policies/ensure-that-alb-drops-http-headers 72 | resource "aws_alb" "asg_alb" { 73 | name = "asg-alb" 74 | internal = false 75 | load_balancer_type = "application" 76 | security_groups = [aws_security_group.alb_sg.id] 77 | subnets = module.vpc.public_subnets 78 | tags = { 79 | Name = "asg-alb" 80 | } 81 | } </failure>
</testcase>
...
```


**✅TASK:** We want to integrate the Checkov logs into the Gitlab Security Dashboard.  More info [here](https://www.checkov.io/8.Outputs/GitLab%20SAST.html) and [here](https://docs.gitlab.com/ee/development/integrations/secure.html#artifacts)


<!-- [Trivy](https://trivy.dev/) by Aqua Security is an open source security scanner, reliable, fast, and easy to use. Use Trivy to find vulnerabilities & IaC misconfigurations, SBOM discovery, Cloud scanning, Kubernetes security risks,and more.

**✅TASK:** Perform the previous tasks using Trivy as well. Compare the results of Checkov and Trivy. Did they find the same issues? -->

### Feedback loops

It is important to have a centralized view of all the security issues that are found in your CI/CD pipeline. This can be done by integrating the security tools that you use into your CI/CD pipeline, and by using a centralized dashboard to view the results.

Gitlab has a Security Dashboard that can be used to view the results of the security tools that are integrated into your CI/CD pipeline. By default, it show the output of the built-in security tools that Gitlab provides, but you can [also integrate other tools into it.](https://docs.gitlab.com/ee/development/integrations/secure.html)

![DevSecOps Feedback loops ](2023-10-26-21-58-39.png){width=50%}



### Want to practice some CI/CD red teaming?

[CI/CD GOAT](https://github.com/cider-security-research/cicd-goat) provides a deliberately vulnerable CI/CD environment. Hack CI/CD pipelines, capture the flags. 🚩

